//log messages in the cloud
const EventEmitter = require('events')

var url = 'http://mylogger.io/log'

//extends allows Logger to be a subclass of Event Emitter and can use everything under event emitter
class Logger extends EventEmitter{

    log = (message) => {
        //Send an HTTP request
    
        console.log(message);
        this.emit('messageLogged', {id: 1, url:'http://'});
    
    
    };
}


//exporting variables out
module.exports = Logger;
// module.exports.endPoint = url;