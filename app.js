// const sayHello = (name) => {
//     console.log("Hello " + name +"!")
// }

// sayHello("Thiru")

//node is a runtime environment

//node module system
//os events fs http


//console is a global object that can be called anywhere
//var defined is not added to global objects
//every file in node is called a module and need to export and import

//app.js is main module
//=========================================

//console.log(module) outputs app.js

//const will prevent accidental overwritting
//jshint app.js can scan errors from terminal

// const logger = require('./logger.js')

// logger.log('Hello this is a message')

//===============================================
//node always wraps code into a function when running 
//module wrapper function


// const path = require('path');

// const pathObj = path.parse(__filename);

// console.log(pathObj)

//============================================

// const os = require('os')

// const totalMem = os.totalmem();
// const freeMem = os.freemem();

// console.log('Total Memory: ' + totalMem)
// console.log(`Total Memory : ${totalMem}`)

//=========================================

// const fs = require('fs');

// const files = fs.readdir('./')

// fs.readdir('./', (err,files) => {
//     if(err) console.log('Error: ' , err);
//     else console.log('Result', files);
// })

//=========================================


// const EventEmitter = require('events')
// const Logger = require('./logger.js')
// const logger = new Logger();
// //need a listener to be called when emit is done
// //listener must be registered first
// logger.on('messageLogged', (arg)=> {
//     console.log('Listener Called', arg);
// });


// logger.log('message');


//===========================================

const http = require('http')

//pass in a callback function
const server = http.createServer((req,res) => {
    if (req.url === '/'){
        res.write('Hello World');
        res.end(); 
    }

//handling multiple routes
    if(req.url === '/api/courses'){
        res.write(JSON.stringify([1,2,3]))
        res.end();
    }
});


server.on('Connection', (socket) => {
    console.log('New Connection...');
});

server.listen(3002)

console.log("Listening on port 3002...")

